export const getDeviceLocation = async (): Promise<Coordinates> =>
  new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => resolve(position.coords),
      reject,
      {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 5000
      }
    );
    setTimeout(reject, 5000);
  });

export const getPostcodeGeoFromCoordinates = async ({
  longitude,
  latitude
}: Coordinates): Promise<string> => {
  const res = await fetch(
    `https://api.postcodes.io/postcodes?lon=${longitude}&lat=${latitude}&radius=1000m&limit=1`
  );
  if (!res.ok || res.status !== 200) throw new Error("Failed to get postcode");
  const data = await res.json();
  if (
    data.status !== 200 ||
    !data.result ||
    !Array.isArray(data.result) ||
    data.result.length === 0
  )
    throw new Error("Failed to validate postcode");
  return data.result[0].postcode;
};

export const getPostcodeGeo = async (
  postcode: string
): Promise<PostcodeResult> => {
  const res = await fetch(`https://api.postcodes.io/postcodes/${postcode}`);
  if (!res.ok || res.status !== 200) throw new Error("Failed to get postcode");
  const data = await res.json();
  if (data.status !== 200 || !data.result) {
    throw new Error("Failed to validate postcode");
  }
  return data.result;
};

export interface PostcodeResult {
  postcode: string;
  quality: number;
  eastings: number;
  northings: number;
  country: string;
  nhs_ha: string;
  longitude: number;
  latitude: number;
  european_electoral_region: string;
  primary_care_trust: string;
  region: string;
  lsoa: string;
  msoa: string;
  incode: string;
  outcode: string;
  parliamentary_constituency: string;
  admin_district: string;
  parish: string;
  admin_county: null;
  admin_ward: string;
  ced: null;
  ccg: string;
  nuts: string;
  codes: Codes;
}

export interface Codes {
  admin_district: string;
  admin_county: string;
  admin_ward: string;
  parish: string;
  parliamentary_constituency: string;
  ccg: string;
  ced: string;
  nuts: string;
}
