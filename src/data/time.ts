import { isSameDay, isBefore, isAfter } from "date-fns";

export const isSameOrEarlierDayThan = (date: Date, comparison = new Date()) =>
  isSameDay(date, comparison) || isBefore(comparison, date);
