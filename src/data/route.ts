export const Routes = {
  POSTCODE: "/",
  SELECT_CONSTITUENCY: "/select-constituency",
  PLEDGE_DETAILS: "/pledge-details",
  THANKS: "/thanks"
};

export const Steps = [
  Routes.POSTCODE,
  Routes.SELECT_CONSTITUENCY,
  Routes.PLEDGE_DETAILS
];
