import { useState, useEffect } from "react";

export const useQuery = <T extends object = any>(
  query: string,
  variables?: any
) => {
  return useFetch<{ data: T; errors?: any[] }>(
    "https://www.mycampaignmap.com/graphql",
    {
      mode: "cors",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query,
        variables
      })
    }
  );
};

export const useFetch = <T extends object = any>(url: string, opts?: any) => {
  const [data, setData] = useState<T>();
  const [loading, setLoading] = useState<boolean>(false);
  const [errors, setErrors] = useState();

  const fetchData = async (url: string, opts: any) => {
    setLoading(true);
    setErrors(undefined);

    try {
      const res = await fetch(url, opts);
      const data = await res.json();
      if (data) {
        setData(data);
      }
    } catch (e) {
      setErrors(e);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData(url, opts);
  }, [url, JSON.stringify(opts)]);

  return { data, loading, errors };
};
