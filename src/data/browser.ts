export const scrollId = (path: string) => `view-${path.replace("/", "")}`;
