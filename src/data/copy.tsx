export const TICKET_TAILOR_URL =
  "https://www.tickettailor.com/events/momentum2";

type BusDestination = {
  constituencyId: string;
  ticketTailorUrl: string;
};

export const BUS_DESTINATIONS: Array<BusDestination> = [
  {
    constituencyId: "E14000834",
    ticketTailorUrl:
      "https://www.tickettailor.com/checkout/view-event/id/327785/chk/8f1a/"
  },
  {
    constituencyId: "E14000653",
    ticketTailorUrl:
      "https://www.tickettailor.com/checkout/view-event/id/327896/chk/9c87/"
  },
  {
    constituencyId: "E14000573",
    ticketTailorUrl:
      "https://www.tickettailor.com/checkout/view-event/id/327800/chk/b740/"
  },
  {
    constituencyId: "E14000578",
    ticketTailorUrl:
      "https://www.tickettailor.com/checkout/view-event/id/327800/chk/b740"
  },
  {
    constituencyId: "E14000761",
    ticketTailorUrl:
      "https://www.tickettailor.com/checkout/view-event/id/327800/chk/b740"
  }
];

export const BUS_CONSTITUENCIES: string[] = [
  // Swansea
  "W07000047",
  "W07000048",
  // Sheffield
  "E14000919",
  "E14000920",
  "E14000921",
  "E14000922",
  "E14000923",
  // Newcastle
  "E14000831",
  "E14000832",
  "E14000833",
  "E14000834",
  // Manchester
  "E14000807",
  "E14000808",
  "E14000809",
  // London
  "E14000687",
  "E14001002",
  "E14000752",
  "E14000750",
  "E14000727",
  "E14000591",
  "E14000592",
  "E14000674",
  "E14000726",
  "E14000629",
  "E14001008",
  "E14000553",
  "E14000615",
  "E14000673",
  "E14000789",
  "E14000718",
  "E14000787",
  "E14000690",
  "E14000869",
  "E14000558",
  "E14000696",
  "E14000540",
  "E14000679",
  "E14001032",
  "E14000882",
  "E14000555",
  "E14000721",
  "E14000764",
  "E14000763",
  "E14000720",
  "E14001013",
  "E14000790",
  // Liverpool
  "E14000796",
  "E14000795",
  "E14000794",
  "E14000793",
  "E14000581",
  // Leeds
  "E14000781",
  "E14000779",
  "E14000778",
  // Hull
  "E14000773",
  "E14000772",
  "E14000771",
  // Glasgow
  "S14000032",
  "S14000035",
  "S14000029",
  "S14000034",
  "S14000030",
  "S14000031",
  "S14000033",
  // Edinburgh
  "S14000024",
  "S14000025",
  "S14000026",
  // Bristol
  "E14000599",
  "E14000600",
  "E14000601",
  "E14000602",
  // Birmingham
  "E14000560",
  "E14000561",
  "E14000562",
  "E14000563",
  "E14000564",
  "E14000565",
  "E14000566",
  "E14000567",
  "E14000568"
];
