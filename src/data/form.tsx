import Postcode from "postcode";
import { StringSchema } from "yup";

export const validatePostcode = (postcode: string) => {
  const p = new Postcode(postcode.trim());
  return Boolean(p.valid() && p.incode()) ? [] : ["Enter a full UK postcode"];
};

export const validateField = (schema: StringSchema<string>) => async (
  value: any
) => isFieldValid(schema, value);

export const isFieldValid = async (
  schema: StringSchema<string>,
  value: string
) => {
  try {
    await schema.validate(value);
    return [];
  } catch (e) {
    return e.errors;
  }
};
