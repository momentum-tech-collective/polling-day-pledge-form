import { isSameOrEarlierDayThan } from "./time";

it("works isSameOrEarlierThan", () => {
  expect(isSameOrEarlierDayThan(new Date())).toBeTruthy();
  expect(isSameOrEarlierDayThan(new Date("2019-12-11"))).toBeTruthy();
  expect(isSameOrEarlierDayThan(new Date("2019-12-08"))).toBeTruthy();
  expect(isSameOrEarlierDayThan(new Date("2019-12-06"))).toBeFalsy();
});
