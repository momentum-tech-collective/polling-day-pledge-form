import ReactGA from "react-ga";

interface Analytics {
  logView: (path: string) => void;
  logEvent: (action: string, context?: any, category?: string) => void;
}

export const initialiseAnalytics = (): Analytics => {
  const key = process.env.REACT_APP_ANALYTICS_ID;
  if (!key) {
    throw new Error(
      "Google analytics key has not been supplied via the REACT_APP_ANALYTICS_ID environment variable."
    );
  }
  ReactGA.initialize(key);
  // Track user events
  initialiseUserTracking();

  const logView: Analytics["logView"] = path => {
    console.info("[Analytics:View]", { path });
    if (process.env.NODE_ENV === "development") return;
    ReactGA.pageview(path);
    mixpanel.track("view" + path);
  };

  const logEvent: Analytics["logEvent"] = (
    action,
    context,
    category = "engagement"
  ) => {
    console.info("[Analytics:Event]", { action, category, context });
    if (process.env.NODE_ENV === "development") return;
    ReactGA.event({ category: category, action: action });
    mixpanel.track(action, context);
  };

  return {
    logView,
    logEvent
  };
};

export const initialiseUserTracking = () =>
  new Promise<Mixpanel>(resolve => {
    // Client-side usage analytics via Mixpanel.com
    mixpanel.init(
      process.env.NODE_ENV === "production"
        ? (process.env.REACT_APP_MIXPANEL_KEY as string)
        : "devkey",
      {
        loaded: mixpanel => {
          resolve(mixpanel);
        }
      }
    );
  });

export const analytics = initialiseAnalytics();
