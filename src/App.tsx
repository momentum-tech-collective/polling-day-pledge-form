/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import React from "react";
import TabBar from "./components/TabBar";
import Footer from "./components/Footer";
import { Swatch } from "./styles";

const App: React.FC = () => {
  return (
    <main
      css={css`
        min-height: 100vh;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
        width: 100%;
      `}
    >
	<React.Fragment>
		<TabBar currentSite={"mpd"} />
	</React.Fragment>
      <div
        css={css`
          min-height: 100%;
          justify-content: center;
          align-items: center;
          height: 100%;
          flex-grow: 1;
          flex-shrink: 0;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          width: 100%;
        `}
      >
		<div css={css`
			text-align: center;
		`}>
			<h1 css={css`
				text-transform: uppercase;
				font-style: italic;
			`}>It all comes down to this.</h1>
			<a css={css`
				font-size: 2em;
				font-weight: bold;
				text-transform: uppercase;
				font-style: italic;
				color: ${Swatch.red};
			`} href='https://mycampaignmap.com'>
				Find out where you're needed &rarr;
			</a>
			<div css={css`
				font-size: 1.5rem;
			`}>
				<p>If you have already signed up to a constituency,
				you should go there.</p>
				<p>Find out where the campaign centres are on <a href='http://mycampaignmap.com'>mycampaignmap.com</a>.</p>
			</div>
		</div>
      </div>
      <div
        css={css`
          margin-bottom: 2em;
          width: 100%;
        `}
      >
        <Footer />
      </div>
    </main>
  );
};

export default App;
