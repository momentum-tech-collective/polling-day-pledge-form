/** @jsx jsx */
import { css, jsx, Global } from "@emotion/core";
import { Fragment } from "react";
import emotionNormalize from "emotion-normalize";

export const Swatch = {
  background: "##FAFAFA",
  pink: "#FFD3FD",
  red: "#E20613",
  rose: "#F0EBEC",
  purple: "#B1048E",
  green: "#04B14A",
  midGrey: "#F0F0F0",
  darkGrey: "#ACACAC",
  black: "#000000",
  darkPink: "#FF57A6",
  white: "#FFFFFF"
};

const defaultLetterSpacing = css`
  letter-spacing: -0.01em;
`;

function calculateRem(size: number) {
  const remSize = size / 16;
  return `${remSize}rem;`;
}

export const font = {
  xlBoldItalic: css`
    font-style: italic;
    font-weight: bold;
    font-size: 30px;
    line-height: 75%;
    ${defaultLetterSpacing}
    text-transform: uppercase;
  `,
  lBold: css`
    font-style: normal;
    font-weight: 500;
    font-size: ${calculateRem(28)};
    line-height: 100%;
    ${defaultLetterSpacing}
  `,
  lRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 28px;
    line-height: 100%;
    ${defaultLetterSpacing}
  `,
  m2Bold: css`
    font-style: normal;
    font-weight: bold;
    font-size: 23px;
    line-height: 18px;
    ${defaultLetterSpacing}
  `,
  m2Medium: css`
    font-style: normal;
    font-weight: 500;
    font-size: 23px;
    @media (max-width: 360px) {
      font-size: 16px;
    }
    line-height: 1.2em;
    ${defaultLetterSpacing}
  `,
  m2Regular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 23px;
    line-height: 1.2em;
    ${defaultLetterSpacing}
  `,
  mBold: css`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 18px;
    ${defaultLetterSpacing}
  `,
  mMedium: css`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    @media (max-width: 360px) {
      font-size: 16px;
    }
    line-height: 1.4em;
    ${defaultLetterSpacing}
  `,
  mRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 1.4em;
    ${defaultLetterSpacing}
  `,
  sMedium: css`
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 16px;
    ${defaultLetterSpacing}
  `,
  sRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 17px;
    ${defaultLetterSpacing}
  `,
  xsRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    letter-spacing: 0.05em;
    text-transform: uppercase;
  `
};

export const buttonStyles = css`
  border-radius: 14px;
  background-color: ${Swatch.red};
  color: ${Swatch.white};
  border: 0;
  padding: 18px;
  text-decoration: none;
  height: 55px;
  width: 100%;
  ${font.mMedium}
  text-transform: uppercase;
  line-height: 1em;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

export const ghostButtonStyles = css`
  border: 2px solid ${Swatch.red};
  color: ${Swatch.red};
  background: white;
  line-height: 0em;
`;

export const textInputCss = css`
  background: #ffffff;

  /* Dark Grey */
  border: 2px solid #828282;
  box-sizing: border-box;
  border-radius: 10px;
  ${font.mBold}
  height: 55px;
  width: 100%;
  padding: 15px;
`;

export const fieldCaptionCss = css`
  font-weight: bold;
  margin: 5px 0;
`;

export const fieldCss = css`
  display: block;
  margin: 30px 0;
  width: 100%;
`;

export const fieldCheckboxCss = css`
  -webkit-appearance: none;
  -moz-appearance: none;
  vertical-align: middle;
  width: 28px;
  height: 28px;
  font-size: 28px;
  background: white;
  border: 2px solid black;
  width: 30px;
  height: 30px;
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;

  &:checked:after {
    content: "";
    background: black;
    height: 70%;
    width: 70%;
  }
`;

export const fieldRadioCss = css`
  -webkit-appearance: none;
  -moz-appearance: none;
  vertical-align: middle;
  width: 28px;
  height: 28px;
  font-size: 60px;
  background: white;
  border: 2px solid black;
  width: 30px;
  height: 30px;
  border-radius: 100%;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  line-height: 0;
  margin-left: 0;
  margin-right: 5px;
  cursor: pointer;

  &:disabled {
    opacity: 0.25;
    cursor: not-allowed;
  }

  &:checked:after {
    content: "•";
    transform: translate(0, 0);
    height: 50%;
    line-height: 0;
  }
`;

export const errorCss = css`
  font-weight: bold;
  color: ${Swatch.red};
  margin-bottom: 10px;
  margin-top: -5px;

  &::first-letter {
    text-transform: uppercase;
  }
`;

export const bigLinkCss = css`
  font-weight: bold;
  color: ${Swatch.red};
`;

export const bigTextCss = css`
  ${font.lRegular}
  margin: 0 0 0.75em;
  @media screen and (max-height: 600px) {
    font-size: 1.3em;
    margin: 0 0 0.5em;
  }
`;

export const mediumTextCss = css`
  ${font.m2Regular}
  margin: 0 0 0.75em;
  @media screen and (max-height: 600px) {
    font-size: 1.3em;
    margin: 0 0 0.5em;
  }
`;

export const heavyTextCss = css`
  ${mediumTextCss}
  font-weight: 600;
`;

export const Layout: React.FC = ({ children }) => (
  <Fragment>
    <Global
      styles={css`
        ${emotionNormalize}

        html,
          body {
          font-family: "neue-haas-grotesk-display";
          font-size: 16px;
        }

        // Set box-sizing to border-box universally
        // See https://css-tricks.com/inheriting-box-sizing-probably-slightly-better-best-practice/
        html {
          box-sizing: border-box;
        }

        *,
        *:before,
        *:after {
          box-sizing: inherit;
        }
      `}
    />
    <div>{children}</div>
  </Fragment>
);
