/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { font, Swatch } from "../styles";

const ProgressBar: React.FC<{
  index: number;
  maxIndex: number;
}> = ({ index, maxIndex }) => {
  const step = index + 1;
  const maxStep = maxIndex + 1;
  const invalidIndex = index < 0 || index > maxIndex;
  return !invalidIndex ? (
    <div
      css={css`
        height: 35px;
        width: 100%;
        position: relative;
        display: flex;
        flex-shrink: 0;
        flex-wrap: 0;
        background: ${Swatch.midGrey};
        position: -webkit-sticky; /* Safari */
        position: sticky;
        top: 0;
      `}
    >
      <div
        css={css`
          height: 100%;
          background: black;
          width: ${(step / maxStep) * 100}%;
          transition: width 1s ease;
        `}
      />
      <div
        css={css`
          height: 100%;
          position: absolute;
          color: white;
          text-transform: uppercase;
          font-style: italic;
          font-weight: 900;
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 0 10px;
        `}
      >
        <span
          css={css`
            font-size: 0.8rem !important;
            line-height: 0.8rem;
          `}
        >
          Step {step} of {maxStep}
        </span>
      </div>
    </div>
  ) : null;
};

export default ProgressBar;
