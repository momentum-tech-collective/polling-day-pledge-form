/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import * as React from "react";
import {
  DetailedHTMLProps,
  InputHTMLAttributes,
  TextareaHTMLAttributes
} from "react";
import { Field as IField, Form as IForm } from "react-jeff";
import { Fragment } from "react";
import { fieldRadioCss } from "../styles";

type ViewElement<Props = {}, Attrs = React.HTMLAttributes<{}>> = React.FC<
  Omit<Attrs, keyof Props> & Props
>;

type IProps = Omit<
  DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  "onChange"
> &
  Partial<IField<string>> & { onChange: (value: string) => void };

export const TextInput = React.forwardRef<HTMLInputElement, IProps>(
  ({ onChange, ...props }: IProps, ref) => {
    return (
      <input
        {...props}
        ref={ref}
        onChange={event => onChange(event.currentTarget.value)}
      />
    );
  }
);

export function LargeTextInput({
  onChange,
  ...props
}: Omit<
  DetailedHTMLProps<
    TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  >,
  "onChange"
> &
  Partial<IField<string>> & { onChange: (v: any) => void }) {
  return (
    <textarea
      {...props}
      onChange={event => onChange(event.currentTarget.value)}
    />
  );
}

export const RadioInput: React.FC<React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>> = ({ children, ...props }) => <input type="radio" {...props} />;

export const RadioGroup: React.FC<{
  options: { value: string; label: string }[];
  field: IField<any>;
}> = ({ options, field }) => {
  return (
    <Fragment>
      {options.map(s => (
        <label
          key={s.value}
          css={css`
            display: flex;
            align-items: center;
            margin: 7px 0;
            cursor: pointer;
          `}
        >
          <RadioInput
            css={fieldRadioCss}
            checked={s.value === field.value}
            value={s.value}
            onChange={e => {
              field.props.onChange(e.currentTarget.value);
            }}
          />
          <span
            css={css`
              margin: 0 5px;
              font-weight: bold;
            `}
          >
            {s.label}
          </span>
        </label>
      ))}
    </Fragment>
  );
};

export const CheckboxInput: React.FC<Omit<
  DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  "value" | "onChange"
> &
  Partial<IField<boolean>> & {
    onChange(v: boolean): void;
  }> = ({ onChange, value, ...props }) => (
  <input
    type="checkbox"
    {...props}
    checked={value}
    onChange={event => onChange(event.currentTarget.checked)}
  />
);

export const Form: ViewElement<
  {},
  React.FormHTMLAttributes<HTMLFormElement>
> = props => (
  <form
    onSubmit={event => {
      event.preventDefault();

      if (props.onSubmit) {
        props.onSubmit(event);
      }
    }}
    {...props}
  />
);

export const FieldErrors: React.FC<{
  style?: any;
  field: IField<any>;
  form?: IForm;
}> = ({ style, field, form }) => {
  const errors =
    (field.errors &&
      // The user moved on to another input,
      !field.focused &&
      // leaving this one in an erroneous state
      (field.touched ||
        // The user tried to submit the form with erroneous state
        (form ? form.submitted : true))) ||
    // Untouched but required
    (field.errors &&
      (form ? form.submitted : true) &&
      field.required &&
      !field.touched)
      ? field.errors
      : null;

  if (errors && errors.length) {
    return <div css={style}>{errors}</div>;
  }

  return null;
};

export const FormErrors: React.FC<{
  style?: any;
  form: IForm;
}> = ({ style, form }) => {
  const errors = form.submitted
    ? [...form.fieldErrors, ...form.submitErrors]
    : null;

  if (errors && errors.length) {
    return (
      <div css={style}>
        <ul>
          {errors.map((e, i) => {
            return <li key={i}>{e}</li>;
          })}
        </ul>
      </div>
    );
  }
  return null;
};
