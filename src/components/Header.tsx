/** @jsx jsx */
import { jsx, css } from "@emotion/core";

export default function Header() {
  return (
    <div>
      <header className="page-width page-content page-header">
        <a className="center" href="https://mypollingday.com/">
          <img
            className="image-no-overflow"
            src="./Header.png"
            alt="Momentum Logo"
          />
        </a>
      </header>
    </div>
  );
}
