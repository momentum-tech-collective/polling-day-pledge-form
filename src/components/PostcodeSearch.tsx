/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { TextInput, Form, FieldErrors } from "../components/FormElements";
import {
  buttonStyles,
  textInputCss,
  fieldCss,
  fieldCaptionCss,
  errorCss,
  font
} from "../styles";
import { Fragment, useRef, useEffect } from "react";
import { Field } from "react-jeff";
import { analytics } from "../data/analytics";

const PostcodeSearch: React.FC<{
  postcodeField: Field<string>;
  onSubmit: (postcode: string) => void;
  focusCursor?: boolean;
}> = ({ postcodeField, onSubmit, focusCursor }) => {
  const submitLocation = (e: any) => {
    e.preventDefault();
    if (postcodeField.valid) {
      onSubmit(postcodeField.value);
    }
  };

  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (inputRef.current && focusCursor === true) {
      console.log({ focusCursor });
      inputRef.current.focus();
    }
  }, [focusCursor, inputRef]);

  return (
    <Fragment>
      <Form
        css={css`
          display: block;
          margin: 10px 0;
        `}
        onSubmit={submitLocation}
      >
        <label
          css={css`
            ${fieldCss}
            margin-bottom: 10px;
          `}
        >
          <div
            css={css`
              ${font.mBold}
              margin-bottom: 8px;
            `}
          >
            What is your full postcode?
          </div>
          <FieldErrors style={errorCss} field={postcodeField} />
          <TextInput
            ref={inputRef}
            css={textInputCss}
            {...postcodeField.props}
            onClick={() => {
              analytics.logEvent("Clicked postcode input");
            }}
            onChange={value => {
              try {
                postcodeField.props.onChange(
                  value.replace(/[ ]{2,}/, " ").toUpperCase()
                );
              } catch (e) {
                console.log(e);
              }
            }}
            autoComplete="postal-code"
          />
        </label>
      </Form>

      <button
        disabled={!postcodeField.valid}
        css={css`
          ${buttonStyles};
          text-transform: uppercase;
        `}
        onClick={submitLocation}
      >
        Find where you're needed
      </button>
      <ul
        css={css`
          opacity: 0.5;
          min-height: 1.5em;
        `}
      >
        {postcodeField.errors.map((e, i) => (
          <li key={i}>{e}</li>
        ))}
      </ul>
    </Fragment>
  );
};

export default PostcodeSearch;
