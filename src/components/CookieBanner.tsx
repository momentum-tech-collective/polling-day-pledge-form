/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import * as React from "react";
import { useCookie } from "@use-hook/use-cookie";
import { Swatch } from "../styles";

const CookieConsentBanner: React.FC<{
  onConsent?: (consented: boolean) => void;
}> = ({ onConsent }) => {
  const [consent, setConsent] = useCookie(
    "my-polling-day-cookie-consent",
    false
  );

  React.useEffect(() => {
    if (onConsent) onConsent(consent);
  }, [consent, onConsent]);

  return !consent ? (
    <div
      css={css`
        opacity: 0.4;
        font-weight: 400;
        border-radius: 10px;
        padding: 10px;
        font-size: 1rem;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        width: 100%;

        @media screen and (min-width: 768px) {
          text-align: center;
        }
      `}
    >
      <span
        css={css`
          font-size: inherit;
        `}
      >
        By using this website you agree to our{" "}
        <a
          css={css`
            font-size: inherit;
            color: inherit;
            text-decoration: none;
            font-weight: 600;
          `}
          href="https://peoplesmomentum.com/privacy-policy/"
        >
          privacy policy
        </a>
        .
      </span>
      <div
        onClick={() => setConsent(true)}
        css={css`
          border: 2px solid ${Swatch.darkGrey};
          color: black;
          border-radius: 5px;
          padding: 7px 10px;
          margin-left: 10px;
          font-size: inherit;
          font-weight: 600;
        `}
      >
        Dismiss
      </div>
    </div>
  ) : null;
};

export default CookieConsentBanner;
