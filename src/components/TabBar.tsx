/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React from "react";

interface ITabProps {
  isActive: boolean;
  textContent: string;
  url: string;
}
const Tab: React.FC<ITabProps> = props => {
  const { isActive, textContent, url } = props;
  return (
    <a
      css={css`
        background-color: ${isActive ? "#3f3f3f" : "#323232"};
        color: ${isActive ? "#ffffff" : "#767676"};
        width: 33.33333%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-weight: 600;
        font-size: 12px;
        text-transform: uppercase;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
      `}
      href={isActive ? undefined : url}
    >
      {textContent}
    </a>
  );
};

export const TAB_BAR_HEIGHT_DESKTOP_PX = 30;
export const TAB_BAR_HEIGHT_MOBILE_PX = 50;

interface ITabBarProps {
  currentSite: "mcm" | "mptw" | "mpd";
}
const TabBar: React.FC<ITabBarProps> = props => {
  const { currentSite } = props;

  return (
    <nav
      css={css`
        height: ${TAB_BAR_HEIGHT_MOBILE_PX}px;
        width: 100%;

        @media screen and (min-width: 1024px) {
          height: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
        }

        display: flex;

        a:nth-child(2) {
          border-left: 2px solid #767676;
          border-right: 2px solid #767676;
        }
      `}
    >
      <Tab
        isActive={currentSite === "mcm"}
        textContent="Campaign Map"
        url="https://mycampaignmap.com?utm_content=tabBar"
      />
      <Tab
        isActive={currentSite === "mptw"}
        textContent="Plan to Win"
        url="https://myplantowin2019.com?utm_content=tabBar"
      />
      <Tab
        isActive={currentSite === "mpd"}
        textContent="Polling Day"
        url="https://www.mypollingday.com?utm_content=tabBar"
      />
    </nav>
  );
};

export default TabBar;
