/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { MdMyLocation } from "react-icons/md";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import { IconType } from "react-icons/lib/cjs";

export const MyLocation: IconType = props => <MdMyLocation {...props} />;

export const LoadingSpinner: IconType = props => (
  <AiOutlineLoading3Quarters
    {...props}
    css={css`
      -webkit-animation: icon-spin 1s infinite linear;
      animation: icon-spin 1s infinite linear;

      @-webkit-keyframes icon-spin {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(359deg);
          transform: rotate(359deg);
        }
      }

      @keyframes icon-spin {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(359deg);
          transform: rotate(359deg);
        }
      }
    `}
  />
);
