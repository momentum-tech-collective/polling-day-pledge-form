/** @jsx jsx */
import { jsx, css } from "@emotion/core";

export default function Footer() {
  return (
    <footer className="page-width page-content page-footer">
      <a className="footer-logo" href="https://peoplesmomentum.com/">
        <img
          css={css`
            width: 140px;
          `}
          className="image-no-overflow"
          src="/momentum.png"
          alt="Momentum Logo"
        />
      </a>
    </footer>
  );
}
