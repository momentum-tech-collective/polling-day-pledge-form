# Instapage Polling Day Pledge Form

This is the way in which we can host a React application on Instapage/

## Deployment

This code is deployed on Heroku by push. When the code is built, head over to [https://polling-day-pledge-form.herokuapp.com](https://polling-day-pledge-form.herokuapp.com)

Add everything between the `<body>` and `<head>` tags to the right bits on Instapage.

You will need to replace the CSS and JS tags location whenever you make a deploy, but you need to do this quickly, as the site will be unavailable momentarily. We can look into how to retain previous builds later - see https://github.com/facebook/create-react-app/issues/5306#issuecomment-431431877

For this reason it is not advisable to set up continuous deployment to the production site.

We also need to kill Instapage CSS. So add this snippet to the Javascript bit of Instapage.

```
<script>
  document.addEventListener("DOMContentLoaded", function() {
  console.log('Ditching Instapage styling'); document.querySelector('style').remove(); })
</script>

```

You need to make sure the Instapage has a form in it. What this script will do is hide the form with simple Javascript to allow it to be submitted.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
